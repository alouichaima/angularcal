import { EspaceService } from './../../../../service/espace.service';
import { Component, OnInit } from '@angular/core';
import { NavigationExtras, Router } from '@angular/router';

@Component({
  selector: 'app-listespace',
  templateUrl: './listespace.component.html',
  styleUrls: ['./listespace.component.css']
})
export class ListespaceComponent implements OnInit {

  list:any;
  constructor(private service:EspaceService  ,private router:Router) { }

  ngOnInit(): void {
    this.getall();
    this.deleteEspace;
  }


  getall():void{

    this.service.getAll().subscribe({next: (data) => {

    this.list= data;

    console.log(data);

    },

    error: (e) => console.error(e)

    });

      }
      deleteEspace(id:number){
        this.service. supprimer(id)
          .subscribe(data => {
            this.deleteEspace=data;
            console.log(data);
          },
            error => console.log(error));

      }
      update(esp:any){
        this.router.navigate(['admin/editespace' ,esp]);
        let navigationExtras:NavigationExtras={

          queryParams:{

          special:JSON.stringify(esp)

          }

          }

          this.router.navigate(['admin/editespace'],navigationExtras);

      }

}
