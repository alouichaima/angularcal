import { EventsService } from 'src/app/service/events.service';
import { Events } from './../../../../../models/events';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-editevents',
  templateUrl: './editevents.component.html',
  styleUrls: ['./editevents.component.css']
})
export class EditeventsComponent implements OnInit {
  id:number =0;
  eve:any={'nomE':'','description':'', 'dateE':'' ,'image':''};

  constructor(private service:EventsService, private router:Router ,private route: ActivatedRoute ) {
    this.route.queryParams.subscribe(params=>{

      if(params&&params.special){

      this.eve=JSON.parse(params.special);



      }

      })
   }
  ngOnInit(): void {


  }
  onSubmit(){

  }

  retour():void{

    this.router.navigate(['admin/list']);

  }
  modif():void{
    this.service.update(this.eve).subscribe({

      next: (data:any)=>{
        this.router.navigate (['admin/list'])

     },

     error: (e:any)=> console.error(e),

     complete:()=>{}

     })

  }

}
