import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AddtarifComponent } from './addtarif.component';

const routes: Routes = [
  {path:'',component:AddtarifComponent}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AddtarifRoutingModule { }
