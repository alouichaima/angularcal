import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { TarifService } from 'src/app/service/tarif.service';

@Component({
  selector: 'app-addtarif',
  templateUrl: './addtarif.component.html',
  styleUrls: ['./addtarif.component.css']
})
export class AddtarifComponent implements OnInit {
  url:any ="";
  ta: any={'nom':'','description':'', 'prix':""};


  constructor(private servicetarif:TarifService,private router:Router) { }

  ngOnInit(): void {
  }

  add(){
    console.log(this.ta);
    this.servicetarif.addtarif(this.ta).subscribe({

       next: (data:any)=>{
         this.router.navigate (['admin/listetarif'])

      },

      error: (t:any)=> console.error(t),

      complete:()=>{}

      })


}

}
