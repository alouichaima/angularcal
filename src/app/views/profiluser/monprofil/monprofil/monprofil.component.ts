import { Component, OnInit } from '@angular/core';
import { NavigationExtras, Router } from '@angular/router';
import { TokenStorageService } from 'src/app/service/token-storage.service';
import { UserCRUDService } from 'src/app/service/user-crud.service';

@Component({
  selector: 'app-monprofil',
  templateUrl: './monprofil.component.html',
  styleUrls: ['./monprofil.component.css']
})
export class MonprofilComponent implements OnInit {

  GymUser: any= {'nom':'','prenom':'', 'datenaiss':'', 'email':'', 'telephone':'', 'poids':''};

  constructor(private serviceuser:UserCRUDService,private token: TokenStorageService,private router:Router) { }

  ngOnInit(): void {
    this.GymUser = this.token.getUser();
  }

  update(us:any){
    this.router.navigate(['profil/editprofil',us]);
    let navigationExtras:NavigationExtras={

      queryParams:{

      special:JSON.stringify(us)

      }

      }

      this.router.navigate(['profil/editprofil'],navigationExtras);

  }

}
