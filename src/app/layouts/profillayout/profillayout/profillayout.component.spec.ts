import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ProfillayoutComponent } from './profillayout.component';

describe('ProfillayoutComponent', () => {
  let component: ProfillayoutComponent;
  let fixture: ComponentFixture<ProfillayoutComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ProfillayoutComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ProfillayoutComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
